package appman.project.oauth2test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpConnect {
	public String getInput(String url, List<NameValuePair> params){
		StringBuilder sb = new StringBuilder();
		InputStream is;
		try{
			// POST to server to get token
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent(); 
			
			// read from inputstream
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			
			String line = "";
			while((line = reader.readLine())!=null){
				sb.append(line+"\n");
			}
			is.close();
		}catch (IOException e){
			
		}
		
		return sb.toString();
	}
	
	public String getInput(String url, String token){
		StringBuilder sb = new StringBuilder();
		InputStream is;
		try{
			// POST to server to get token
			DefaultHttpClient httpClient = new DefaultHttpClient();
			
			HttpGet httpGet = new HttpGet(url);
			httpGet.addHeader("Authorization","Bearer "+token);
			
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent(); 
			
			// read from inputstream
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			
			String line = "";
			while((line = reader.readLine())!=null){
				sb.append(line+"\n");
			}
			is.close();
		}catch (IOException e){
			
		}
		
		return sb.toString();
	}
	
}
