package appman.project.oauth2test;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends Activity {
	
	private final String URL = "http://dipify.appmanproject.com/api/chat/info";
	private String prefToken;
	private TextView textView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		
		// key of preferences
		String key = getResources().getString(R.string.package_name);
		// Create preferences
		final SharedPreferences prefs = this.getSharedPreferences(key,
				Context.MODE_PRIVATE);
		// read token from preferences
		prefToken = prefs.getString(key, "none");
		Log.i("pref token 2", prefToken);
		
		textView = (TextView) findViewById(R.id.textView);
		textView.setText(prefToken);
		
		Button logoutBtn = (Button) findViewById(R.id.logout_btn);
		logoutBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				prefs.edit().clear().commit();
				finish();
			}
		});
		
		Button chatInfo = (Button) findViewById(R.id.get_chat_info);
		chatInfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				new Test().execute();
			}
		});
	}
	
	private class Test extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {
			return new HttpConnect().getInput(URL, prefToken);
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			textView.setText(result);
		}
		
	}
}
