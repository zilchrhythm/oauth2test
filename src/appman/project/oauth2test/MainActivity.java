package appman.project.oauth2test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	private EditText username, password;
	private Button loginBtn;
	
	private final String URL = "http://dipify.appmanproject.com/oauth/mobiletoken";
	
	private Context mContext;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        String key = getResources().getString(R.string.package_name);
		// Create preferences
		SharedPreferences prefs = getSharedPreferences(key, Context.MODE_PRIVATE);
		
		
		
		if(!prefs.contains(key)){
			mContext = this;

			username = (EditText) findViewById(R.id.username);
			password = (EditText) findViewById(R.id.password);
			loginBtn = (Button) findViewById(R.id.login_btn);

			loginBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					new Test().execute();
				}
			});
		} else {
			startActivity(new Intent().setClass(this, SecondActivity.class));
			finish();
		}
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    private class Test extends AsyncTask<String, String, String>
    {

		@Override
		protected String doInBackground(String... params) {

			// params for get token
			List<NameValuePair> parameters = new ArrayList<NameValuePair>();
			parameters.add(new BasicNameValuePair("username",username.getText().toString()));
			parameters.add(new BasicNameValuePair("password",password.getText().toString()));
			parameters.add(new BasicNameValuePair("grant_type","password"));
			parameters.add(new BasicNameValuePair("client_id", "51d4fa930f8e9cddbfbf985e"));
			parameters.add(new BasicNameValuePair("client_secret", "ssh-secret"));
			parameters.add(new BasicNameValuePair("scope", "chat"));
			
			return new HttpConnect().getInput(URL, parameters);
		}
		// result from doExecute() return as json string
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.i("json", result);

			String token = "";
			
			try {
				// change string to json object
				JSONObject json = new JSONObject(result);
				// get only access token
				token = json.getString("access_token");
			} catch (JSONException e) {
				e.printStackTrace();
			}			
			
			Log.i("token",token);
			
			// key of preferences
			String key = mContext.getResources().getString(R.string.package_name);
			// Create preferences
			SharedPreferences prefs = mContext.getSharedPreferences(key, Context.MODE_PRIVATE);
			// put token into preferences
			prefs.edit().putString(key, token).commit();
			// read token from preferences
			String prefToken = prefs.getString(key, "");
			Log.i("pref token",prefToken);
			
			startActivity(new Intent().setClass(mContext, SecondActivity.class));
		}
    	
    }
}

